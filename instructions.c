/*
 * instructions.c
 *
 */

#include <string.h>

#include "assembler.h"

/* Registers */
static char *registers[] = {
	"zero",
	"at",
	"v0", "v1",
	"a0", "a1", "a2", "a3",
	"t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7",
	"s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7",
	"t8", "t9",
	"k0", "k1",
	"gp", "sp", "fp", "ra"
};

static int register_number(char *name)
{
	int i;
	char *pos = &name[1]; /* '$' */

	for (i = 0; i < sizeof(registers) / sizeof(char *); i++)
		if (!strcmp(pos, registers[i]))
			return i;
	return -1;
}

__u32 _la(char *op)
{
	return 0;
}

__u32 _lui(char *op)
{
	return 0;
}

/* _lw
 * [I]
 */
__u32 _lw(char *op)
{
	I_format_t instruction;
	char *immediate, *rs, *rt;

	rt = parse_token(op, ",", &op, NULL);
	immediate = parse_token(op, " ,()\t", &op, NULL);
	rs = parse_token(op, " ()\t", &op, NULL);

	memset((void *) &instruction, 0x00, sizeof(instruction));
	instruction.opcode = 0x23;
	instruction.rs = register_number(rs);
	instruction.rt = register_number(rt);
	instruction.immediate = atoi(immediate);

	free(rs);
	free(immediate);
	free(rt);

	return *((__u32*) &instruction);
}

/* _sw
 * [I]
 */
__u32 _sw(char *op)
{
	I_format_t instruction;
	char *immediate, *rs, *rt;

	rt = parse_token(op, ",", &op, NULL);
	immediate = parse_token(op, " ,()\t", &op, NULL);
	rs = parse_token(op, " ()\t", &op, NULL);

	memset((void *) &instruction, 0x00, sizeof(instruction));
	instruction.opcode = 0x2b;
	instruction.rs = register_number(rs);
	instruction.rt = register_number(rt);
	instruction.immediate = atoi(immediate);

	free(rs);
	free(immediate);
	free(rt);

	return *((__u32*) &instruction);
}

__u32 _add(char *op)
{
	R_format_t instruction;
	char *rd, *rs, *rt;

	rd = parse_token(op, ", ", &op, NULL);
	rs = parse_token(op, ", ", &op, NULL);
	rt = parse_token(op, ", ", &op, NULL);

	memset((void *) &instruction, 0x00, sizeof(instruction));
	instruction.opcode = 0x40;
	instruction.rd = register_number(rd);
	instruction.rs = register_number(rs);
	instruction.rt = register_number(rt);

	free(rt);
	free(rs);
	free(rd);

	return *((__u32*) &instruction);
}

__u32 _sub(char *op)
{
	R_format_t instruction;
	char *rd, *rs, *rt;

	rd = parse_token(op, ", ", &op, NULL);
	rs = parse_token(op, ", ", &op, NULL);
	rt = parse_token(op, ", ", &op, NULL);

	memset((void *) &instruction, 0x00, sizeof(instruction));
	instruction.opcode = 0x44;
	instruction.rd = register_number(rd);
	instruction.rs = register_number(rs);
	instruction.rt = register_number(rt);

	free(rt);
	free(rs);
	free(rd);

	return *((__u32*) &instruction);
}

__u32 _addi(char *op)
{
	I_format_t instruction;

	memset((void *) &instruction, 0x00, sizeof(instruction));
	instruction.opcode = 0x08;

	return *((__u32*) &instruction);
}

__u32 _or(char *op)
{
	return 0;
}

__u32 _and(char *op)
{
	return 0;
}

__u32 _ori(char *op)
{
	return 0;
}

__u32 _andi(char *op)
{
	return 0;
}

__u32 _slt(char *op)
{
	return 0;
}

__u32 _slti(char *op)
{
	return 0;
}

__u32 _sll(char *op)
{
	return 0;
}

__u32 _srl(char *op)
{
	return 0;
}

__u32 _beq(char *op)
{
	return 0;
}

__u32 _bne(char *op)
{
	return 0;
}

__u32 _j(char *op)
{
	return 0;
}

__u32 _jr(char *op)
{
	return 0;
}

__u32 _jal(char *op)
{
	return 0;
}

__u32 _nop(char *op)
{
	return 0;
}


