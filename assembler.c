/*
 * assembler.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "assembler.h"

/* Input/output files */
static FILE *fpin;
static FILE *fpout;

/* Instruction hash table */
static hash_table_t *instruction_table;

#if 0
/* Data/Variable hash table */
hash_table_t *data_table;
data_record_t *data_records[DEFAULT_HASH_SIZE];
__u32 ndata_records;

/* Label hash table */
hash_table_t *label_table;
#endif

/* Line buffer */
#define	LINEBUFSIZE				256
static char linebuf[LINEBUFSIZE];
static __u32 linenum;

static void assembler_exit(void)
{
	if (fpin)
		fclose(fpin);
	if (fpout)
		fclose(fpout);
	if (data_table)
		destroy_hash_table(data_table);
	if (label_table)
		destroy_hash_table(label_table);
	if (instruction_table)
		destroy_hash_table(instruction_table);
}

static int assembler_init(char *input, char *output)
{
	int res = 0;

	if ((fpin = fopen(input, "r")) == NULL) {
		res = errno;
		goto errout;
	}
	if ((fpout = fopen(output, "w")) == NULL) {
		res = errno;
		goto errout;
	}

	if ((data_table = create_hash_table(DEFAULT_HASH_SIZE)) == NULL) {
		res = errno;
		goto errout;
	}
	if ((label_table = create_hash_table(DEFAULT_HASH_SIZE)) == NULL) {
		res = errno;
		goto errout;
	}
	if ((instruction_table = create_hash_table(N_INSTRUCTIONS)) == NULL) {
		res = errno;
		goto errout;
	}

	/* FIXME:
	 * - Try some other way than this. We have to check for every insert!
	 * - Get rid of all strlen!
	 */
	hash_insert(instruction_table, (void *) "la", 	strlen("la"),	_la);
	hash_insert(instruction_table, (void *) "lui", 	strlen("lui"),	_lui);
	hash_insert(instruction_table, (void *) "lw", 	strlen("lw"),	_lw);
	hash_insert(instruction_table, (void *) "sw", 	strlen("sw"),	_sw);
	hash_insert(instruction_table, (void *) "add", 	strlen("add"),	_add);
	hash_insert(instruction_table, (void *) "sub", 	strlen("sub"),	_sub);
	hash_insert(instruction_table, (void *) "addi", strlen("addi"),	_addi);
	hash_insert(instruction_table, (void *) "or", 	strlen("or"),	_or);
	hash_insert(instruction_table, (void *) "and", 	strlen("and"),	_and);
	hash_insert(instruction_table, (void *) "ori", 	strlen("ori"),	_ori);
	hash_insert(instruction_table, (void *) "andi",	strlen("andi"),	_andi);
	hash_insert(instruction_table, (void *) "slt", 	strlen("slt"),	_slt);
	hash_insert(instruction_table, (void *) "slti",	strlen("slti"),	_slti);
	hash_insert(instruction_table, (void *) "sll", 	strlen("sll"),	_sll);
	hash_insert(instruction_table, (void *) "srl", 	strlen("srl"),	_srl);
	hash_insert(instruction_table, (void *) "beq", 	strlen("beq"),	_beq);
	hash_insert(instruction_table, (void *) "bne", 	strlen("bne"),	_bne);
	hash_insert(instruction_table, (void *) "j", 	strlen("j"),	_j);
	hash_insert(instruction_table, (void *) "jr", 	strlen("jr"),	_jr);
	hash_insert(instruction_table, (void *) "jal", 	strlen("jal"),	_jal);
	hash_insert(instruction_table, (void *) "nop", 	strlen("nop"),	_nop);

	return 0;

errout:
	assembler_exit();
	return res;
}

static char wordbuf[33];

static char *get_binary_string_word(int number)
{
	__u32 mask = 0x80000000;
	char *pos = wordbuf;

	for ( ; mask > 0; mask >>= 1, pos++) {
		*pos = (mask & number) ? '1' : '0';
	}
	wordbuf[32] = '\0';

	return wordbuf;
}

/* NOTE: caller should free the string returned! */
static char *get_binary_string_char(char ch)
{
	__u8 mask = 0x80;
	char *buffer = malloc(sizeof(char) * 9);
	char *pos = buffer;

	for ( ; mask > 0; mask >>= 1, pos++) {
		*pos = ((__u8) mask & (__u8) ch) ? '1' : '0';
	}
	pos[8] = '\0';

	return buffer;
}

static void dump_data_section(void)
{
	int i, j;
	int words_written = 0;

	for (i = 0; i < ndata_records; i++) {
		data_record_t *current = data_records[i];

		if (current->type == DATA_TYPE_WORD) {
			for (j = 0; j < current->length / 4; j++) {
				fprintf(fpout, "%s\n", get_binary_string_word(current->value.number));
				words_written++;
			}
		}
		else {
			char *str = current->value.string;
			char *tmp[4];
			int len = strlen(str);
			for (j = 0; j < len; j++) {
				int k = j % 4;
				tmp[k] = get_binary_string_char(str[j]);

				/* NOTE that we also have to handle the partially filled word. */
				if (k == 3 || j + 1 == len) {
					int padding = 3 - k;

					while (padding--)
						fputs("00000000", fpout);

					do {
						fprintf(fpout, "%s", tmp[k]);
						free(tmp[k]);
					} while (k--);
					fputc('\n', fpout);

					words_written++;
				}
			}
		}
	}

	/* pad to 1024 words.. */
	for (i = 0; i < 1024 - words_written; i++)
		fputs("00000000000000000000000000000000\n", fpout);
}

static int build_machine_code(void)
{
	int mode = MODE_TEXT;
	__u32 code;

	linenum = 0;
	rewind(fpin);

	while (fgets(linebuf, LINEBUFSIZE-1, fpin) != NULL) {
		char *token = linebuf;
		linenum++;

		if (strlen(linebuf) > LINEBUFSIZE-1)	/* Ignore if the line is too long.. */
			continue;

		while (1) {
			char *current = parse_token(token, "\n\t ", &token, NULL);

			if (current == NULL || *current == '#') {
				free(current);
				break;
			}

			printf("%s\n", current);

			if (mode == MODE_TEXT) {
				__u32 (*func)(char *);

				if (!strcmp(".text", current)) {
					free(current);
					break;
				}
				if (!strcmp(".data", current)) {
					mode = MODE_DATA;
					free(current);
					break;	/* We can safely ignore rest of the tokens.. */
				}

				if (current[strlen(current) - 1] == ':') {
					/* insert label table entry.. */

					break;
				}

				/* Other cases, just parse the instruction! */
				func = hash_find(instruction_table,
							current, strlen(current));
				if (func == NULL) {
					fprintf(stderr, "Parse error on line %d, %s\n",
							linenum, current);
					free(current);
					return -1;
				}
				code = func(token);
				fprintf(fpout, "%s\n", get_binary_string_word(code));
				/*fwrite((void *) &code, sizeof(code), 1, fpout);*/
				break;
			}
			else {	/* MODE_DATA */
				if (!strcmp(".text", current)) {
					mode = MODE_TEXT;
					free(current);
					break;
				}
			}
		}
	}
	if (ferror(fpin))
		return errno;

	fputc('\n', fpout);

	/* Dump data section */
	dump_data_section();

	return 0;
}


static int build_data_table(void)
{
	int mode = MODE_TEXT;
	data_record_t *data_record;

	while (fgets(linebuf, LINEBUFSIZE-1, fpin) != NULL) {
		char *token = linebuf;
		linenum++;

		if (strlen(linebuf) > LINEBUFSIZE-1)	/* Ignore if the line is too long.. */
			continue;

		while (1) {
			char *current = parse_token(token, "\n\t ", &token, NULL);

			if (current == NULL || *current == '#') {
				free(current);
				break;
			}

			printf("%s\n", current);

			if (mode == MODE_TEXT) {
				if (!strcmp(".text", current)) {
					free(current);
					break;
				}
				if (!strcmp(".data", current)) {
					mode = MODE_DATA;
					free(current);
					break;	/* We can safely ignore rest of the tokens.. */
				}
			}
			else {	/* MODE_DATA */
				if (!strcmp(".text", current)) {
					mode = MODE_TEXT;
					free(current);
					break;
				}

				if (current[strlen(current) - 1] == ':') {
					data_record = (data_record_t *) malloc(sizeof(data_record_t));
					current[strlen(current) - 1] = '\0';
					data_record->name = current;
					continue;
				}

				if (!strcmp(".word", current)) {
					char *str = token;
					char *pos = str;

					while (*pos != '\0') {
						if (isspace(*pos)) {
							*pos = '\0';
							break;
						}
						pos++;
					}

					pos = strchr(str, ':');
					if (pos == NULL) {
						data_record->length = 4;
						data_record->value.number = atoi(str);
					}
					else {
						*pos = '\0';
						pos++;
						data_record->length = 4 * atoi(pos);
						data_record->value.number = atoi(str);
					}

					data_record->type = DATA_TYPE_WORD;

					hash_insert(data_table, data_record->name,
							strlen(data_record->name), data_record);
					data_records[ndata_records++] = data_record;

					break;
				}

				if (!strcmp(".asciiz", current)) {
					char *str = token + 1;
					char *pos = strchr(str, '"');

					*pos = '\0';

					data_record->type = DATA_TYPE_ASCII;
					data_record->length = strlen(str);
					data_record->value.string = strdup(str);

					hash_insert(data_table, data_record->name,
							strlen(data_record->name), data_record);
					data_records[ndata_records++] = data_record;

					break;
				}
			}
		}
	}
	if (ferror(fpin))
		return errno;

	return 0;
}

int main(int argc, char **argv)
{
	if (argc != 3) {
		fprintf(stderr, "Usage: %s <asm file> <output file>\n",
			argv[0]);
		return 0;
	}

	if (assembler_init(argv[1], argv[2]) < 0) {
		perror("Initialization failed!");
		return errno;
	}

	if (build_data_table() < 0) {
		perror("build_data_table failed");
		return errno;
	}

	if (build_machine_code() < 0) {
		perror("build_machine_code failed");
		return errno;
	}

	assembler_exit();

	return 0;
}

