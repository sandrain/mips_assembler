#ifndef	__ASSEMBLER_H__
#define	__ASSEMBLER_H__

#include <linux/types.h>

#include "hash_table.h"
#include "tokenizer.h"

/* Data table entry. */
struct data_record_st {
	char *name;
	int type;
#define	DATA_TYPE_WORD		0
#define	DATA_TYPE_ASCII		1
	int length;
	__u32 offset;	/* offset from 0x2000.. */
	union {
		char *string;
		int number;
	} value;
};
typedef struct data_record_st		data_record_t;

/* Label table entry. */
struct label_record_st {
	char *name;
	__u32 offset;	/* offset from 0x00.. */
};
typedef struct label_record_st		label_record_t;

/* Instrution table entry. */
struct instruction_record_st {
	char *name;
	__u32 (*handler)(char *);
};
typedef struct instruction_record_st	instruction_record_t;

/* Instruction formats. */
struct R_format_st {
	int funct	: 6;
	int shamt	: 5;
	int rd		: 5;
	int rt		: 5;
	int rs		: 5;
	int opcode	: 6;
};
typedef struct R_format_st	R_format_t;

struct I_format_st {
	int immediate	: 16;
	int rt		: 5;
	int rs		: 5;
	int opcode	: 6;
};
typedef struct I_format_st	I_format_t;

struct J_format_st {
	int address	: 26;
	int opcode	: 6;
};
typedef struct J_format_st	J_format_t;

/* Instruction handlers. */
#define	N_INSTRUCTIONS			19
extern __u32 _la(char *op);
extern __u32 _lui(char *op);
extern __u32 _lw(char *op);
extern __u32 _sw(char *op);
extern __u32 _add(char *op);
extern __u32 _sub(char *op);
extern __u32 _addi(char *op);
extern __u32 _or(char *op);
extern __u32 _and(char *op);
extern __u32 _ori(char *op);
extern __u32 _andi(char *op);
extern __u32 _slt(char *op);
extern __u32 _slti(char *op);
extern __u32 _sll(char *op);
extern __u32 _srl(char *op);
extern __u32 _beq(char *op);
extern __u32 _bne(char *op);
extern __u32 _j(char *op);
extern __u32 _jr(char *op);
extern __u32 _jal(char *op);
extern __u32 _nop(char *op);


#define	DEFAULT_HASH_SIZE		512

#define	MODE_TEXT		0
#define	MODE_DATA		1

hash_table_t *data_table;
data_record_t *data_records[DEFAULT_HASH_SIZE];
__u32 ndata_records;
hash_table_t *label_table;

#include <errno.h>
extern int errno;

#endif	/* __ASSEMBLER_H__ */

