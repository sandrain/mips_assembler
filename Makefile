#
# Makefile
#

CC = gcc
CFLAGS = -g -Wall

TARGET = assembler

%: %.o
	$(CC) $(CFLAGS) -o  $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

all: $(TARGET)

clean:
	rm -f *.o $(TARGETS)

assembler: instructions.o assembler.o
	$(CC) $(CFLAGS) -o $@ $^

